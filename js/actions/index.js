import ReviewAssetMetadataApi from '../api/ReviewAssetMetadataApi';
import * as types from '../constants/ReviewConstants';

export function populateReviewForm() {
  return dispatch => {
  ReviewAssetMetadataApi.get_RMD_Data().then(function(data) {
    dispatch({
        type: types.REVIEW_METADATA,
        RMD_Data: JSON.parse(data.text),
        success: true
      })
      }, function(error) {
        dispatch({
          type: types.REVIEW_METADATA_ERROR,
          RMD_Data_Err: error.message,
          success: false
        })
      }).catch(e => {
        dispatch({
          type: types.REVIEW_METADATA_ERROR,
          RMD_Data_Err: e.message,
          success: false
        })
      })
}
}

export function saveReviewForm(values) {
  return dispatch => {
  ReviewAssetMetadataApi.save_RMD_Data(values).then(function(data){
        dispatch({
        type: types.SAVE_METADATA,
        RMD_Data: JSON.parse(data.text),values,
        success: true
      })
      },function(error){
        dispatch({
          type: types.SAVE_METADATA_ERROR,
          RMD_Data_Err: error.message,
          success: false
        })
      }).catch(e => {
        dispatch({
          type: types.SAVE_METADATA_ERROR,
          RMD_Data_Err: e.message,
          success: false
        })
    })
}
}
