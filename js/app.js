import 'babel-polyfill'
import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import store from './store';
import App from './components/App'
import { addLocaleData, IntlProvider } from 'react-intl';
import frLocaleData from 'react-intl/locale-data/fr';

addLocaleData(frLocaleData);

const translations = {
	'fr': {
			 "Review_Asset_MetaData": "Métadonnées du produit",			 		 
		  }
};

const locale = document.documentElement.getAttribute('lang');;
const filename = window.location.pathname.split('.')[0];
render(
  <IntlProvider locale={locale} messages={translations[locale]}>
  <Provider store={store}>
    <App filename={filename} />
  </Provider></IntlProvider>, document.getElementById('root')
)
