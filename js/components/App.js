import React from 'react';
import ReviewAssetMetadata from '../containers/ReviewAssetMetadataContainer';

const App = (props) => (
  <div>
    <ReviewAssetMetadata  filename={props.filename}/>  
  </div>
)

export default App
