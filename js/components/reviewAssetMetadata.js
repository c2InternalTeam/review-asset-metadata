import React, { Component, PropTypes } from 'react'
import Label from'./../components/Label';
import Heading from './../components/Heading';
import TextBox from './../components/TextBox';
import TextArea from './../components/TextArea';
import Image from './../components/image';
import TagElem from './../components/TagElem';
import {reduxForm} from 'redux-form';
import FormMessages from 'redux-form-validation';
import {generateValidation} from 'redux-form-validation';
import {injectIntl, intlShape} from 'react-intl';
import {messages} from './ReviewAssetMetadataDefaultMessages';
export const fields = ['astName','uuid','orgFileName','description','altText',
'figCaption','transcript','objAlign','closedCaption',
'copyRight','tags','URN','fileName','contentType','PAFID','contextSpecific'];
  

var validations = {
     copyRight: {
       required: true
     },
     'astName': false,
     'uuid': false,
     'orgFileName': false,
     'description': false,
     'altText': false,
     'figCaption': false,
     'closedCaption':false,
     'transcript':false,
     'objAlign':false,
     'tags': false,
     'contextSpecific': false,
     'URN':false

};

class reviewAssetMetadata extends React.Component {
    static PropTypes = {
        intl: intlShape.isRequired
    }

  constructor(props) {
    super();
    this.displayName = 'ReviewAssetMedataComp';
    this.componentWillMount = props.componentWillMount;
    this.onSave = props.onSave;
    this.state = {
        showToggle: false,
        showHide:'none',
        fields:{
        	contextSpecific: props.fields.contextSpecific
        }
    }
    this.ShowContextSpecific =  this.ShowContextSpecific.bind(this);
    this.HideContextSpecific =  this.HideContextSpecific.bind(this);  

    

}
ShowContextSpecific(e){      
   this.setState({showToggle: !this.state.showToggle})
}

HideContextSpecific(e){      
   this.setState({showToggle: !this.state.showToggle})
   //this.setState({fields:{contextSpecific:this.props.fields.contextSpecific}} 
    //debugger;
   	this.props.fields.contextSpecific.value = '';
   	//debugger;
   	//this.render();

 }

 showHide(){    
    if(this.state.showHide == 'none'){
    	this.setState({showHide:'table-row'});	
	}else{	
		this.setState({showHide:'none'});	
	}   
 }

componentWillReceiveProps(nextProps){
	flowplayer.conf.embed = false;
	CKEDITOR.instances.assetNameCk.setData(nextProps.assetName);
			$("#reviewAssestAudio").flowplayer({
		splash: true,
		flashls: {
		startfromlevel: 0
		}
		});
}
 
componentDidMount(){		

		CKEDITOR.disableAutoInline = true;	

		let instanceReadOnlyfalse = function(ev) {
			var editor = ev.editor;
			editor.setReadOnly( false );	
		}

		let instanceReadOnlyTrue = function(ev) {
			var editor = ev.editor;
			editor.setReadOnly( true );	
		}

		let ckeditConfig = {
		    enterMode : CKEDITOR.ENTER_BR,
            shiftEnterMode: CKEDITOR.ENTER_P,				
			extraPlugins: 'sharedspace,sourcedialog',			
			sharedSpaces: {
			top: 'ckContainer',				
			},
			toolbarGroups: [
			{"name":"basicstyles","groups":["basicstyles"]},				
			{"name":"insert","groups":['Specialchar']},				
			],			
			removeButtons: 'Image,Anchor,Styles,Specialchar,Table,HorizontalRule'
		} 


        const  CKEditior = ['altTextCk','figCaptionCk','closedCaptionCk','assetNameCk','copyRightCk','transcriptCk'];

        for(let key in CKEditior){                      
	        CKEDITOR.inline(CKEditior[key], ckeditConfig ).on('focus', instanceReadOnlyfalse);	        
			CKEDITOR.instances[CKEditior[key]].on('blur', instanceReadOnlyTrue);
			CKEDITOR.instances[CKEditior[key]].on('instanceReady', instanceReadOnlyTrue);
        }

		/*flowplayer.conf.embed = false;

		$("#reviewAssestAudio").flowplayer({
		splash: true,
		flashls: {
		startfromlevel: 0
		}
		});*/

 }
  render() { 
  	const divName = {
						/*position: 'relative',
						top: -58,
						left: 960,*/
						display:'table-cell',
						verticalAlign:'middle',
						//textAlign: 'center',
						padding:0
    				 }

		const divAltText = {
							    /*position: 'relative',
							    left: 960,
							    top: -58,*/
								display:'table-cell',
								verticalAlign:'middle',
								//textAlign: 'center',
								padding:0
						}
		const divSP = {
					/*position: 'relative',
					left: 960,
					top:-48*/
				    padding:0,
					display: 'table-cell',
					verticalAlign: 'middle',
					//textAlign: 'center'
				}


		const flowplayer =  {
		   width: 600,
		   height: 338
		}

		const flowplayerAudio =  {
		width: 600,		
		}

      const {formatMessage} = this.props.intl;
	   const {
	    fields:{
	      astName, uuid, orgFileName,
	      description, altText, figCaption, copyRight, 
	      transcript, closedCaption, objAlign, tags,URN,fileName,contentType,PAFID,contextSpecific},
	      handleSubmit
	    } = this.props;
          
        /* const {
         	fields:{contextSpecific}
	    } = this.props;*/

       //debugger;
       

        let displayAssest = '';
        let displayAssestTest = '';

        if(this.props.mimetype != undefined && this.props.mimetype == 'audio/mpeg'){
        	displayAssest =  <div className="pe-input pe-input--horizontal">
        					 <div id="reviewAssestAudio" data-audio-only="true" className="playful" style={flowplayerAudio}>
							<video>			 	
							<source type="audio/mp3" src="/Kalimba.mp3" />
							</video>				
							</div>
							</div>;
        	
        }

        if(this.props.mimetype != undefined && this.props.mimetype != 'video/mp4'){
        	displayAssestTest =  <div className="pe-input pe-input--horizontal">				
					<div className="flowplayer" style={flowplayer}>
					<video>				
					<source type="video/mp4" src="/Currency.mp4" />
					</video>
					</div>
				</div>;
        	
        }

		if(this.props.mimetype != undefined && this.props.mimetype == 'image/jpeg'){
        	displayAssest =  <div className="pe-input pe-input--horizontal">
                    <Image name="images/Cat.PNG" alt="Smiley face" height="300" width="300"/>
                </div>;        	
        }
        
        let displayAssestVideo = '';
        let displayAssestAudio = '';
		let displayAssestImage = '';

        if(this.props.filename === '/video'){
        		displayAssestVideo = <div className="pe-input pe-input--horizontal">				
					<div className="flowplayer" style={flowplayer}>
					<video>				
					<source type="video/mp4" src="/Currency.mp4" />
					</video>
					</div>
				</div>;	
        }
        
        if(this.props.filename === '/audio'){
		displayAssestAudio = <div className="pe-input pe-input--horizontal">
        					 <div id="reviewAssestAudio" data-audio-only="true" className="playful" style={flowplayerAudio}>
							<video>			 	
							<source type="audio/mp3" src="/Kalimba.mp3" />
							</video>				
							</div>
							</div>;
        }
        
		if(this.props.filename === '/image'){
				displayAssestImage = <div className="pe-input pe-input--horizontal">
                    <Image name="images/Cat.PNG" alt="Smiley face" height="300" width="300"/>
                </div>;
		}
		       
		       if(this.props.filename === '/'){
        		displayAssestVideo = <div className="pe-input pe-input--horizontal">				
					<div className="flowplayer" style={flowplayer}>
					<video>				
					<source type="video/mp4" src="/Currency.mp4" />
					</video>
					</div>
				</div>;	

				displayAssestAudio = <div className="pe-input pe-input--horizontal">
				 <div id="reviewAssestAudio" data-audio-only="true" className="playful" style={flowplayerAudio}>
				<video>			 	
				<source type="audio/mp3" src="/Kalimba.mp3" />
				</video>				
				</div>
				</div>;

				displayAssestImage = <div className="pe-input pe-input--horizontal">
                    <Image name="images/Cat.PNG" alt="Smiley face" height="300" width="300"/>
                </div>;				

        }


    //const handleClick = () => alert('Clicked!');

		
    return (
        <div>
        <form>
         <div className="pe-assetmetadata">
         <section>
            <div className="pe-input pe-input--horizontal" >
            <Label />
            <h2>{formatMessage(messages.Review_Asset_MetaData)}</h2>                       
            </div>
			
			<div className="pe-input pe-input--horizontal" >
			<Label />
			{displayAssestVideo}
			   {displayAssestAudio}
			   {displayAssestImage}		
			</div>

			<div className="pe-input pe-input--horizontal" >
			<Label />
			 <div id="ckContainer" style={{width:230}}>		
			</div>  	   
                   
				</div>

				<div className="pe-input pe-input--horizontal" >
			<Label />
			 <h2>{formatMessage(messages.Asset_Metadata)}</h2>
			</div>
                
                <div  className="pe-metadata-mvm">

                <div className="pe-input pe-input--horizontal" >
                        <Label for="assetName" text={formatMessage(messages.Asset_Name)}  />
                        {/*<TextBox type="text" value={astName} disabled={false}/>*/}                        
                        
                        <div contentEditable="true" id="assetNameCk"></div>

                        <div style={divName} >
                        <img onClick={this.showHide.bind(this)} src="images/Link.PNG" alt="Smiley face" height="50" width="50"/>
                        </div>

                </div>
               

                <div className="pe-input pe-input--horizontal" style={{display:this.state.showHide}}>
                        <Label for="URN" text= 'URN'/>
                        <TextBox id="URN" disabled={true}  placeholder="URN" value={URN}/>
                </div> 

                  <div className="pe-input pe-input--horizontal" style={{display:this.state.showHide}}>
                        <Label for="fileName" text='File Name'/>
                        <TextBox id="fileName" disabled={true}  placeholder="fileName"  value={fileName} />

                </div> 
                  <div className="pe-input pe-input--horizontal" style={{display:this.state.showHide}}>
                        <Label for="contentType" text='Content Type'/>
                        <TextBox id="contentType" disabled={true} placeholder="contentType"  value={'contentType'} />

                </div> 
                  <div className="pe-input pe-input--horizontal" style={{display:this.state.showHide}}>
                        <Label for="PAFID" text='PAF ID'/>
                        <TextBox id="PAFID" disabled={true} placeholder="PAFID"  value={'PAFID'} />

                </div> 


                <div className="pe-input pe-input--horizontal">
                        <Label for="UUID" text={formatMessage(messages.UUID)}/>
                        <TextBox id="uuid" disabled={true} value={uuid} />

                </div> 
                  <div className="pe-input pe-input--horizontal">
                        <Label for ="orgFileName" text={formatMessage(messages.Original_File_Name)}/>
                        <TextBox  id="orgFileName" disabled={true} value={orgFileName}/>
                </div >              
                <div className="pe-input pe-input--horizontal">
                         <Label for="Description" text={formatMessage(messages.Description)}/>
                         <TextArea  value ={description} id="descId" placeholder="Description"/>

                </div>
                 
                <div className="pe-input pe-input--horizontal">
                         <Label for ="altText" text={formatMessage(messages.Alt_Text)}/>
                         {/* <TextBox id="altText" value={altText} placeholder="Add Alt Text"/> */}
                         <div contentEditable="true" id="altTextCk" ></div>
                         <div style={divAltText}>
                          <img onClick = {this.ShowContextSpecific} src="images/Link.PNG" alt="Smiley face" height="50" width="50"/>
                         </div>
                </div>
                {this.state.showToggle ? 
                <div className="pe-input pe-input--horizontal" >                                         
                    <Label for="Description" text="Content Specific Alt Text"/>
                    <TextBox id="ShowContextSpecific" type="text" placeholder="Content Specific Alt Text" value={contextSpecific}  />
                    <div style={divSP}>
                    <img onClick = {this.HideContextSpecific} src="images/delete.PNG" alt="Smiley face" height="30" width="30" />                      
                    </div>
                 </div>
                 : ''}               
                 <div className="pe-input pe-input--horizontal">
                         <Label for="FigCaption" text={formatMessage(messages.Figure_Caption)}/>
                         {/*<TextBox id="figCaption" placeholder="Add Caption" value={figCaption}/>*/}
                         <div contentEditable="true" id="figCaptionCk" ></div>
                          
                </div>          
                <div className="pe-input pe-input--horizontal navcontainer">
                         <Label for ="copyRight" text={formatMessage(messages.Copy_Right)}/>
                         {/*
                         <TextBox id="copyRight" required={false} placeholder="Add Copyright information" value={copyRight} />
                        
                          <FormMessages tagName="ul" errorCount="1" field={copyRight}>
                            <li when="required">
                                  Error:required field
                            </li>
                         </FormMessages>
                         */
                        }

                        <div contentEditable="true" id="copyRightCk" ></div>
                </div>
                <div className="pe-input pe-input--horizontal">
                         <Label for="Transcript" text={formatMessage(messages.Transcript)}/>
                         {/*<TextBox id="transcript" placeholder="Add transcript" value={transcript}/>*/}
						 <div contentEditable="true" id="transcriptCk" ></div>
                </div>
                <div className="pe-input pe-input--horizontal">
                         <Label for="ClosedCaption" text={formatMessage(messages.ClosedCaption)}/>
                         {/*<TextBox id="closedCaption" placeholder="Add closed captions" value={closedCaption}/>*/}
                         <div contentEditable="true" id="closedCaptionCk" ></div>   
                </div>
                <div className="pe-input pe-input--horizontal">
                         <Label for="ObjectiveAlignment" text={formatMessage(messages.ObjectiveAlignment)} />
                         <TextBox id="objAlign" disabled={false} placeholder="Add in Learning Objective URI" value={objAlign}/>
                         {/*<div contentEditable="true" id="objAlignCk"></div> */}
                </div>
                       
                <div className="pe-input pe-input--horizontal">
                	<Label for="KeyWord" text={'KeyWord'}/>
                  <TagElem suggestions={this.props.suggestions} tags={tags.value}/>
                </div>
                <div className="pe-input pe-input--horizontal" style={{textAlign:'right'}}>
                  <Label />
                  <button onClick={handleSubmit(this.onSave)} style={{marginTop:10}}>{formatMessage(messages.Save)}</button>               
                </div>
				<div className="pe-input pe-input--horizontal" style={{textAlign:'right'}}>
				     <Label />
					 <button style={{margin:20}}>{formatMessage(messages.Cancel)}</button>
	                 <button>{formatMessage(messages.Import)}</button>
	            </div>
  </div>       
        </section>

            </div>
        </form>
        </ div>
    );
  }
}

reviewAssetMetadata= reduxForm(
  {
    form: 'ReviewAsssetMetadata',
    fields,
    //...generateValidation(validations)
  }
)(reviewAssetMetadata);


export default injectIntl(reviewAssetMetadata);