import { connect } from 'react-redux';
import { populateReviewForm, saveReviewForm } from '../actions';
import ReviewAssetMetadata from '../components/reviewAssetMetadata';

const getSelectedValues = (dataArray) => {
  if (dataArray.length > 0) {
    return dataArray[dataArray.length-1];
  }

  return [];
}

const mapStateToProps = (state) => {
  const data = getSelectedValues(state.ReviewAssetReducer);
  return {
    suggestions: data.suggestions,
    errMsg: data.errMsg,
    mimetype:data.mimetype,    
    url:data.url,
    assetName:data.astName,
    "initialValues": {
    astName: data.astName,
    uuid: data.uuid,
    orgFileName: data.orgFileName,
    description: data.description ,
    altText: data.altText,
    figCaption: data.FigCaption,
    keyword: data.keyword,
    transcript: data.transcript,
    closedCaption: data.closedCaption,
    copyRight: data.copyRight,
    objAlign: data.objAlign,
    tags: data.tags,
    contextSpecific: "",
    URN:"",
    fileName:"",
    contentType:"",
    PAFID:"",    
  }
  }
}

const mapDispatchToProps = (dispatch) => {
  return {

    componentWillMount() {
      dispatch(populateReviewForm())
    },
     onSave(values, dispatch){
      console.log("inside onSave");
      dispatch(saveReviewForm(values));
    }
  }
}

const ReviewAssetMetadataContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(ReviewAssetMetadata)

export default ReviewAssetMetadataContainer
