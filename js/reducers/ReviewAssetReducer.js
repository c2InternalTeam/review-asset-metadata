import * as types from '../constants/ReviewConstants';
import Immutable from 'immutable';

const initilizeValues = [{
  astName: '',
  uuid: '',
  orgFileName: '',
  description: '',
  altText: '',
  figCaption: '',
  copyRight: '',
  keyword: '',
  transcript: '',
  closedCaption: '',
  objAlign: '',
  suggestions: [],
  tags: [],
  errMsg: ''
}]

const ReviewAssetReducer = (state = initilizeValues, action) => {
let newState = {};
let retRevData = {};
let retSavedata = {};
try {
  switch (action.type) {
    case types.REVIEW_METADATA:
    if (!state && !state[0]) {
      newState = [
          Immutable.fromJS(state[0]).merge(Immutable.Map(action.RMD_Data)).toJS()
      ]
    }else{
    newState = [
    Immutable.fromJS({
    astName: '',
    uuid:'',
    orgFileName: '',
    description: '' ,
    altText: '',
    figCaption: '',
    copyRight: '',
    suggestions: [],
    tags: [],
    errMsg: '' }).merge(Immutable.Map(action.RMD_Data)).toJS()
    ]
  }
  break;
  case types.SAVE_METADATA:
  if (!state && !state[0]) {
      newState = [
        Immutable.fromJS(state[0]).merge(Immutable.Map(action.RMD_Data)).toJS()
      ]
    }else{
  newState = [
    Immutable.fromJS({
    astName: '',
    uuid:'',
    orgFileName: '',
    description: '' ,
    altText: '',
    figCaption: '',
    copyRight: '',
    suggestions: [],
    tags: [],
    errMsg: ''}).merge(Immutable.Map(action.RMD_Data)).toJS()
    ]
   }
   break;
  case types.REVIEW_METADATA_ERROR:
    retRevData = Object.assign({}, state[0], {
            errMsg: action.RMD_Data_Err
          });
    newState = [retRevData];
    break;
  case types.SAVE_METADATA_ERROR:
      retSavedata = Object.assign({}, state[0], {
            errMsg: action.RMD_Data_Err
          });
    newState = [retSavedata];
    break;
  default:
      newState = state;
    break;
  }
}catch (e){
  console.error('error',e);
}
return newState;
}

export default ReviewAssetReducer
