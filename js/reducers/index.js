import { combineReducers } from 'redux';
import ReviewAssetReducer from './ReviewAssetReducer';
import { reducer as formReducer } from 'redux-form';

const reviewAssetMetaData = combineReducers({
  ReviewAssetReducer,
  form: formReducer
})

export default reviewAssetMetaData
