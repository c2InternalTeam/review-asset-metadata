import * as actions from '../../js/actions/index'
import expect from 'expect' 
import reviewMetadataConstants from '../../js/constants/ReviewConstants';
import reviewAssetMetadataApi from '../../js/api/ReviewAssetMetadataApi';
import store from '../../js/store';
import Promise from 'bluebird';

describe('async actions', () => {
  var dispacthSpy;
  var apiSpy;

  before(function () {
     dispacthSpy = sinon.spy(store,"dispatch");
     apiSpy = sinon.spy(reviewAssetMetadataApi,"get_RMD_Data");
  });

 it('should return a function', () => {
    expect(actions.populateReviewForm()).toBeA('function');
  })

 it('api should be called when fetching RMD data', () => {
    actions.populateReviewForm()(store.dispatch);
    expect(apiSpy.called).toEqual(true);
 })

  after(function () { 
    dispacthSpy.restore();
    apiSpy.restore();
  });
});


describe('stub api and returning sucess should work', () => {
  var dispacthSpy;
  let data;
  before(function () {
    data = {"rmddata":"abcdefg"};
    data = JSON.stringify(data);
     sinon.stub(reviewAssetMetadataApi, "get_RMD_Data").returns(Promise.resolve({"text":data}));
     dispacthSpy = sinon.spy(store,"dispatch");
  });
  it('dispatch a RMDData action', () => {
    actions.populateReviewForm()(store.dispatch);
    setTimeout(function(){
      let dispatchArg = dispacthSpy.args[0][0];
      expect(dispacthSpy.called).toEqual(true);
      expect(dispatchArg.type).toEqual(reviewMetadataConstants.REVIEW_METADATA);
      expect(dispatchArg.RMD_Data.rmddata).toEqual("abcdefg");
      
    },1000)
  });
  after(function () { 
     dispacthSpy.restore();
     reviewAssetMetadataApi.get_RMD_Data.restore();
  });
});


describe('stub api and returning error should work', () => {
  var dispacthSpy1;
  let data;
  before(function () {
    data = {"rmddata":"abcdefg"};
    data = JSON.stringify(data);
  
     sinon.stub(reviewAssetMetadataApi, "get_RMD_Data").returns(Promise.reject({"error":"error"}));
     dispacthSpy1 = sinon.spy(store,"dispatch");
  });
  it('dispatch a RMDData action', () => {
    actions.populateReviewForm()(store.dispatch);
  
    setTimeout(function(){
      expect(dispacthSpy1.called).toEqual(false); 
    },1000)
  });
  after(function () { 
    dispacthSpy1.restore();
     reviewAssetMetadataApi.get_RMD_Data.restore();
  });
});

describe('async actions', () => {
  var dispacthSpy;
  var server;

  before(function () {
     dispacthSpy = sinon.spy(store,"dispatch");
     server = sinon.fakeServer.create();
     server.respondWith("GET", "http://localhost:3000/metaData", [
    200, 
    {"Content-Type": "application/json"}, 
    JSON.stringify([{"uuid":"d123654"}])
    ]);

   
  });


  it('fectch RMD should work fine when mocking service', () => {
    actions.populateReviewForm()(store.dispatch);
    server.respond();
    console.log("server respond called");
    setTimeout(function(){
      expect(dispacthSpy.called).toEqual(true);
    },100)
  });

  after(function () { 
    dispacthSpy.restore();
    server.restore(); 
  });
});
describe('async actions', () => {
  var dispacthSpy;
  var apiSpy;

  before(function () {
     dispacthSpy = sinon.spy(store,"dispatch");
     apiSpy = sinon.spy(reviewAssetMetadataApi,"save_RMD_Data");
  });

 it('should return a function', () => {
    expect(actions.saveReviewForm()).toBeA('function');
  })

 it('api should be called when fetching RMD data', () => {
    actions.saveReviewForm()(store.dispatch);
    expect(apiSpy.called).toEqual(true);
 })

  after(function () { 
    dispacthSpy.restore();
    apiSpy.restore();
  });
});


describe('stub api and returning sucess should work', () => {
  var dispacthSpy;
  let data;
  before(function () {
    data = {"rmddata":"abcdefg"};
    data = JSON.stringify(data);
     sinon.stub(reviewAssetMetadataApi, "save_RMD_Data").returns(Promise.resolve({"text":data}));
     dispacthSpy = sinon.spy(store,"dispatch");
  });
  it('dispatch a RMDData action', () => {
    actions.saveReviewForm()(store.dispatch);
    setTimeout(function(){
      let dispatchArg = dispacthSpy.args[0][0];
      expect(dispacthSpy.called).toEqual(true);
      expect(dispatchArg.type).toEqual(reviewMetadataConstants.SAVE_METADATA);
      expect(dispatchArg.RMD_Data.rmddata).toEqual("abcdefg");
      
    },1000)
  });
  after(function () { 
     dispacthSpy.restore();
     reviewAssetMetadataApi.save_RMD_Data.restore();
  });
});


describe('stub api and returning error should work', () => {
  var dispacthSpy1;
  let data;
  before(function () {
    data = {"rmddata":"abcdefg"};
    data = JSON.stringify(data);
  
     sinon.stub(reviewAssetMetadataApi, "save_RMD_Data").returns(Promise.reject({"error":"error"}));
     dispacthSpy1 = sinon.spy(store,"dispatch");
  });
  it('dispatch a RMDData action', () => {
    actions.saveReviewForm()(store.dispatch);
   
    setTimeout(function(){
      expect(dispacthSpy1.called).toEqual(false); 
    },1000)
  });
  after(function () { 
    dispacthSpy1.restore();
     reviewAssetMetadataApi.save_RMD_Data.restore();
  });
});

describe('async actions', () => {
  var dispacthSpy;
  var server;

  before(function () {
     dispacthSpy = sinon.spy(store,"dispatch");
     server = sinon.fakeServer.create();
     server.respondWith("GET", "http://localhost:3000/saveMetaData", [
    200, 
    {"Content-Type": "application/json"}, 
    JSON.stringify([{"uuid":"d123654"}])
    ]);

 });


  it('fectch RMD should work fine when mocking service', () => {
    actions.saveReviewForm()(store.dispatch);
    server.respond();
    console.log("server respond called");
    setTimeout(function(){
      expect(dispacthSpy.called).toEqual(true);
    },100)
  });

  after(function () { 
    dispacthSpy.restore();
    server.restore(); 
  });
});

