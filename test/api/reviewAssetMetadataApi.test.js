import expect from 'expect' //used any testing library
import reviewAssetMetadataApi from '../../js/api/ReviewAssetMetadataApi';
import Promise from 'bluebird';



describe('API', () => {
  var server;

  before(function () {
     server = sinon.fakeServer.create();
     server.respondWith("GET", "http://localhost:3000/metaData", [
    404, 
    {"Content-Type": "application/json"}, 
    JSON.stringify([{"uuid":"d123654"}])
    ]);
  });

  it('fectch RMD should work fine when mocking service', () => {
    let servicecall = reviewAssetMetadataApi.get_RMD_Data();
    server.respond();
    
    servicecall.then(function(data){ 
        console.log("data", data);
        expect(data.text).toEqual(JSON.stringify([{"uuid":"d123654"}]));
      }, function(error){
        console.log("error 33333 -->",error);
      });
  });


  after(function () { 
    server.restore(); 
  });

});


describe('async actions', () => {
  var server;

  before(function () {
     server = sinon.fakeServer.create();
     server.respondWith("GET", "http://localhost:3000/saveMetaData", [
    200, 
    {"Content-Type": "application/json"}, 
    JSON.stringify([{"uuid":"d123654"}])
    ]);
  });
it('save RMD should work fine when mocking service', () => {
    let servicecall = reviewAssetMetadataApi.save_RMD_Data();
    server.respond();
   
    servicecall.then(function(data){ 
        console.log("data", data);
        expect(data.text).toEqual(JSON.stringify([{"uuid":"d123654"}]));
      }, function(error){
        console.log("error 33333 -->",error);
      });
  });

  after(function () { 
    server.restore(); 
  });

});


