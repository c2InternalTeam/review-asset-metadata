let React = require('react');
let ReactDOM = require('react-dom');
let TestUtils = require('react-addons-test-utils'); 
let Image = require('../../js/components/image'); 
let chai = require('chai');
let expect = chai.expect;

describe('Image  test cases', function () {
   before('before creating tags', function() {
       this.imageComp = TestUtils.renderIntoDocument(<Image name="images/Cat.PNG" text="Smiley face" height="300" width="300"/>);
 
  });

it('should render correctly', function () {
    let tag = TestUtils.scryRenderedDOMComponentsWithTag(this.imageComp, 'img');
    let todoLength = tag.length;
    let src=ReactDOM.findDOMNode(this.imageComp).getAttribute("src") 
    let alt=ReactDOM.findDOMNode(this.imageComp).getAttribute("alt") 
    let height=ReactDOM.findDOMNode(this.imageComp).getAttribute("height")
    let width=ReactDOM.findDOMNode(this.imageComp).getAttribute("width")  

    expect(src).to.equal("images/Cat.PNG");
    expect(alt).to.equal("Smiley face");
    expect(height).to.equal("300");
    expect(width).to.equal("300");
    expect(tag.length).to.equal(1);
  });

});