let React = require('react');
let ReactDOM = require('react-dom');
let TestUtils = require('react-addons-test-utils'); 
let Tag = require('../../js/components/TagElem'); 
let chai = require('chai');
let expect = chai.expect;

describe('TagElem  test cases', function () {
   before('before creating tags', function() {
    let props = {

       tags: [
                { id: 1, name: "Apples" },
                { id: 2, name: "Appless" }
                
            ],
            suggestions: [
                { id: 4, name: "Mangos" },
                { id: 5, name: "Lemons" },
                { id: 6, name: "Apricots" }
            ]
    } 
    this.props = props;
    this.tagComp = TestUtils.renderIntoDocument(<Tag  suggestions={this.props.suggestions} tags= {this.props.tags}/>);
  });


  it('should render correctly', function () {
    let tag = TestUtils.scryRenderedDOMComponentsWithTag(this.tagComp, 'button');
    let todoLength = tag.length;
    expect(tag.length).to.equal(2);
  });


it('should  added tag, when we click new item ', function () {
  let tagItem ={ id: 4, name: "Mangos" };

this.tagComp.handleAddition(tagItem);

let buttons=TestUtils.findRenderedDOMComponentWithClass(this.tagComp, 'ReactTags__selected')
let spans = buttons.querySelectorAll('span')


    expect(this.tagComp.props.tags.length).to.equal(3);
  });


it('should  deleted tag, when we click an item ', function () {

this.tagComp.handleDelete(0);

let buttons=TestUtils.findRenderedDOMComponentWithClass(this.tagComp, 'ReactTags__selected')
let spans = buttons.querySelectorAll('span')

   expect(this.tagComp.props.tags.length).to.equal(2);
  });
 
});