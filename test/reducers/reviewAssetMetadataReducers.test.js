import reviewConstants from '../../js/constants/ReviewConstants';
import reviewAssetReducer from '../../js/reducers/ReviewAssetReducer'
import reviewAssetMetadataApi from '../../js/api/ReviewAssetMetadataApi'
import * as actions from '../../js/actions/index'
import expect from 'expect'

describe('reviewAsset reducer', () => {

  let initilizeValues =[{
    astName: '',
    uuid:'',
    orgFileName: '',
    description: '' ,
    altText: '',
    figCaption: '',
    copyRight: '',
    suggestions: [],
    tags: [] ,
    errMsg: ''
}];


before('before creating ', function() {

this.RMD_Data={
  "astName":"111ui1",
  "uuid":"Chapter Quiz:The Course of Empire:Expansion and",
  "orgFileName":"Question1",
  "description":"Journal",
  "altText":"Professor",
  "figCaption":"Medium",
  "copyRight":"Align Type 2",
  "suggestions":[],
  "tags":[],
  "errMsg": ''
};
});

  it('should handle initial state', () => {
   
  let ret = reviewAssetReducer(undefined,{});
    expect(ret).toEqual([{
    astName: '',
    uuid:'',
    orgFileName: '',
    description: '' ,
    altText: '',
    figCaption: '',
    copyRight: '',
    suggestions: [],
    tags: [] ,
    errMsg: ''}]);
  });

  it('should handle RMD change', () => {debugger;

    let nextState = reviewAssetReducer({}, {
        type:reviewConstants.REVIEW_METADATA,
        RMD_Data:{
            "astName":'astName',
            "uuid":'123456',
            "orgFileName":'orgFileName',
            "description":'description',
            "altText":'altText',
            "figCaption":'figCaption',
            "copyRight":'copyRight',
            "suggestions":[],
            "tags":[],
            "errMsg": ''
          }

    });

    expect(nextState).toEqual([{
           "astName":'astName',
            "uuid":'123456',
            "orgFileName":'orgFileName',
            "description":'description',
            "altText":'altText',
            "figCaption":'figCaption',
            "copyRight":'copyRight',
            "suggestions":[],
            "tags":[],
            "errMsg": ''
          }]);

    });


it('should handle RMD save change', () => {debugger;

    let nextState = reviewAssetReducer({}, {
        type:reviewConstants.SAVE_METADATA,
        RMD_Data:{
            "astName":'astName',
            "uuid":'123456',
            "orgFileName":'orgFileName',
            "description":'description',
            "altText":'altText',
            "figCaption":'figCaption',
            "copyRight":'copyRight',
            "suggestions":[],
            "tags":[],
            "errMsg": ''
          }

    });

    expect(nextState).toEqual([{
            "astName":'astName',
            "uuid":'123456',
            "orgFileName":'orgFileName',  
            "description":'description',
            "altText":'altText',
            "figCaption":'figCaption',
            "copyRight":'copyRight',
            "suggestions":[],
            "tags":[],
            "errMsg": ''
          }]);

    });

  });
